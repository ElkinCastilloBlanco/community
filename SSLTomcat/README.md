# Install SSL Certicate in Tomcat using let's Encrypt


## Step 01 Install Java JDK
yum update  
yum upgrade  

yum install java 

## Step 02 Install Tomcat

wget https://dlcdn.apache.org/tomcat/tomcat-8/v8.5.78/bin/apache-tomcat-8.5.78.tar.gz  

mkdir /opt/tomcat  
tar xvf apache-tomcat-8*tar.gz -C /opt/tomcat --strip-components=1  
cd /opt/tomcat  
cd bin  

Start up tomcat
./startup.sh

Shutdown up tomcat
./shutdown.sh

## Step 03 Change Tomcat Default port 8080 to 80 

Change Tomcat Default port 8080  to 80 to automatically redirect smartbill.us (tomcat webpage)  
Edit server.xml file  
/opt/tomcat/conf/server.xml   
Check url:  
www.smartbill.us  
https://www.smartbill.us   

`<Connector executor="tomcatThreadPool"
port="80" protocol="HTTP/1.1"
connectionTimeout="20000"
redirectPort="443" />`

## Step 03 Install Let's encrypt component

 yum install epel-release  
 certbot certonly --standalone -d tomcat.tecadmin.net   
 ls /etc/letsencrypt/live/tomcat.tecadmin.net/   
 cert.pem  chain.pem  fullchain.pem  privkey.pem  README  
 cp *.pem /opt/tomcat/conf/   

 vi /opt/tomcat/conf/server.xml   
 apply certifaction files  

/opt/conf/server.xml  
 `<Connector port="443" protocol="org.apache.coyote.http11.Http11NioProtocol"
               maxThreads="150" SSLEnabled="true">
        <SSLHostConfig>
            <Certificate certificateFile="conf/cert.pem"
                 certificateKeyFile="conf/privkey.pem"
                 certificateChainFile="conf/chain.pem" />
        </SSLHostConfig>
 </Connector>`

/opt/conf/web.xml    

 `<security-constraint>
   <web-resource-collection>
    <web-resource-name>Protected Context</web-resource-name>
    <url-pattern>/*</url-pattern>
    </web-resource-collection>
    <user-data-constraint>
    <transport-guarantee>CONFIDENTIAL</transport-guarantee>
    </user-data-constraint>
 </security-constraint>` 


 
## Add your files

```

```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/ElkinCastilloBlanco/community/-/settings/integrations)
